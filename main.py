import os
import requests
import zipfile
import shutil
import tempfile

from cloudfoundry_client.client import CloudFoundryClient
from flask import Flask, request

app = Flask(__name__)
app.debug = True

@app.route('/')
def index():
    return 'it works'

@app.route('/pullandpush', methods=['POST'])
def pullandpush():
    repo = request.form['repo']
    r = requests.get(repo, stream=True)
    with tempfile.NamedTemporaryFile(delete=False) as repozip:
        for chunk in r.iter_content(chunk_size=8192):
            repozip.write(chunk)

    with zipfile.ZipFile(repozip.name, 'r') as source:
        topdir = source.namelist()[0]
        with tempfile.NamedTemporaryFile(delete=False) as out:
            with zipfile.ZipFile(out, 'w') as outz:
                for path in source.namelist()[1:]:
                    with tempfile.NamedTemporaryFile(delete=False) as pfft:
                        with source.open(path, 'r') as fp:
                            shutil.copyfileobj(fp, pfft)
                    outz.write(pfft.name, path[len(topdir):])
                    os.unlink(pfft.name)
        os.unlink(repozip.name)
        push(request.form, out.name)
        return "Let's make it happ'n, capt'n!"
    print f.name
    return f.name

def get_cf_client(d):
    target_endpoint = d.get('cf_endpoint', 'https://api.run.pivotal.io')
    username = d['cf_username']
    password = d['cf_password']
    proxy = dict(http=os.environ.get('HTTP_PROXY', ''), https=os.environ.get('HTTPS_PROXY', ''))
    client = CloudFoundryClient(target_endpoint, proxy=proxy, skip_verification=False)
    print username,password
    client.init_with_user_credentials(username, password)
    return client

def push(d, fname):
    client = get_cf_client(d)
    app = client.apps.get_first(name=d['app_name'])
    app_guid = app['metadata']['guid']
    client.apps.put_bits(app_guid, open(fname, 'rb'))
    client.apps.restage(app_guid)
#    client.apps.stop(app_guid)
#    client.apps.start(app_guid)
